#ifndef _TASK1_H
#define _TASK1_H

#include "FreeRTOS.h"
#include "globals.h"
#include "task.h"

#define T1_REGION_SIZE_IN_BYTES 1024

void task1(void * params);

#endif
