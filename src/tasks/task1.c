#include "task1.h"
#include <stdlib.h>
#include <math.h>

/* data */
extern uint8_t t1Region[]; /* import region from main.c */

/* prototypes */
static void writeToMem(uint8_t *start, uint16_t size);

xMemoryRegion readOnly[portNUM_CONFIGURABLE_REGIONS] = {
	{
		.pvBaseAddress = t1Region,
		.ulLengthInBytes = T1_REGION_SIZE_IN_BYTES,
		.ulParameters = portMPU_REGION_READ_ONLY,
	},
	{0,0,0},
	{0,0,0}
};

void task1(void * params)
{
	//debug_printf("task1 started\n\r");
	while(1)
	{
		/* This should work */
		writeToMem(&(t1Region[0]), T1_REGION_SIZE_IN_BYTES);
		//debug_printf("it did\n\r");

		/* This should crash */
		/* NULL means modify the calling task */	
		vTaskAllocateMPURegions(NULL, readOnly);
        /* YIELD is necessary - settings are effectiv only after a context switch! 
         * - not documented! "§$$"§$"§!
         */
        taskYIELD();

		//debug_printf("This write should NOT work...");
		writeToMem(&(t1Region[0]), T1_REGION_SIZE_IN_BYTES);
	}
}

void writeToMem(uint8_t *start, uint16_t size)
{
	uint16_t i = 0;
	uint8_t write = 1;
	
	for(i = 0; i < size; i++)
	{
		*(start + i) = write;
	}
}
