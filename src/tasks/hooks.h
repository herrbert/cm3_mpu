#ifdef _HOOKS_H_
#define _HOOKS_H_

void vApplicationStackOverflowHook(xTaskHandle xTask, 
        signed portCHAR *pcTaskName);

#endif
