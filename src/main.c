#include "FreeRTOS.h"
#include "task.h"

#include "task1.h"
//#include "assert.h"

#define T1_STACK_SIZE 128

static portSTACK_TYPE t1Stack[T1_STACK_SIZE] __attribute__((aligned(T1_STACK_SIZE*sizeof(portSTACK_TYPE))));
uint8_t t1Region[T1_REGION_SIZE_IN_BYTES];

#define AHB_PERIPHERALS 0x20080000
#define AHB_PERIPHERALS_SIZE 0x3FFF

#define APB_PERIPHERALS 0x40000000
#define APB_PERIPHERALS_SIZE 0xFFFF


#define M3_PERIPHERALS 0xE0000000 
#define M3_PERIPHERALS_SIZE 0xFFFFF

xTaskParameters taskConfig = {
	.pvTaskCode = task1,
	.pcName = "t1",
	.usStackDepth = T1_STACK_SIZE,
	.pvParameters = (void *)NULL,
	.uxPriority = 2, //| portPRIVILEGE_BIT, /* ( 2 | portPRIVILEGE_BIT ) for prio */
	.puxStackBuffer = t1Stack,
	.xRegions =
	{
		{
			.pvBaseAddress = t1Region,
			.ulLengthInBytes = T1_REGION_SIZE_IN_BYTES,
			.ulParameters = portMPU_REGION_READ_WRITE,
		},
        {0,0,0},
        {0,0,0}
	}
};

int main(void)
{
	debug_printf("Application started\n\r");
	xTaskCreateRestricted(&taskConfig, NULL);
	
        debug_printf("starting the scheduler...\n\r");
        vTaskStartScheduler();

        /* will only get here if there was insufficent mem
         * for idle task */
	debug_printf("something went somewhere terribly wrong!\n\r");
        //assert(0);
    while(1) {};
}

