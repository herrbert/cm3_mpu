#include "globals.h"
#include "hooks.h"
#include "FreeRTOS.h"
#include "task.h"
void vApplicationStackOverflowHook(xTaskHandle xTask, 
	signed portCHAR *pcTaskName )
{
	debug_printf("STACK OVERFLOW DETECTED\n\r");
	debug_printf("overflowed task: %s", pcTaskName);
	debug_printf("hanging...");
	while(1);
}
